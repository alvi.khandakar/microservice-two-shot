from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO
# Do we need to create an acls to import our picture?

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href"
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "bin",
        "picture_url",
    ]
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}



class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin"
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
	"""
	Lists manufacturer and model name
	{
		"shoes": [
			{
			"manufacturer": "x",
			"model_name": "y",
			},
			...
		]
	}
	"""
	if request.method == "GET":
		shoes = Shoe.objects.all()
		return JsonResponse(
			{"shoes": shoes},
			encoder=ShoeListEncoder,
			safe=False
		)
	else: # POST request
		content = json.loads(request.body)
		print(content)
		if "bin" in content:
			try:
				bin_href = content["bin"]
				bin = BinVO.objects.get(import_href=bin_href)
				content["bin"] = bin
			except BinVO.DoesNotExist:
				return JsonResponse(
					{"message": "invalid bin id"},
					status=400,
				)
		shoe = Shoe.objects.create(**content)
		return JsonResponse(
			shoe,
			encoder=ShoeDetailEncoder,
			safe=False,
		)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, pk):
	if request.method == "GET":
		shoes = Shoe.objects.get(id=pk)
		return JsonResponse(
			shoes,
			encoder=ShoeDetailEncoder,
			safe=False,
		)
	elif request.method == "DELETE":
		count, _ = Shoe.objects.filter(id=pk).delete()
		return JsonResponse({"delete": count > 0})
	else: # PUT request
		content = json.loads(request.body)
		Shoe.objects.filter(id=pk).update(**content)
		shoes = Shoe.objects.get(id=pk)
		return JsonResponse(
			shoes,
			encoder=ShoeDetailEncoder,
			safe=False
		)