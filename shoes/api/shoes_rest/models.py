from django.db import models
from django.urls import reverse



class BinVO(models.Model):
  closet_name = models.CharField(max_length=200)
  bin_number = models.PositiveSmallIntegerField()
  bin_size = models.PositiveSmallIntegerField()
  import_href = models.CharField(
    max_length=200, null=True, blank=True, unique=True)


class Shoe(models.Model):
  manufacturer = models.CharField(max_length=200)
  model_name = models.CharField(max_length=200)
  color = models.CharField(max_length=200)
  picture_url = models.URLField()
  bin = models.ForeignKey(
    BinVO,
    related_name = "bins",
    null=True,
    on_delete=models.CASCADE,
  )