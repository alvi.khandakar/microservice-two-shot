# Wardrobify

Team:

* Alvi Khandakar - Shoes
* Nate Seon - Hats

## Design

WILL FIX LATER
![alt text](http://url/to/img.png)

![alt text](https://github.com/[username]/[reponame]/blob/[branch]/image.jpg?raw=true)


## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.



## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

on hats.api.hats_project: 
 - .settings.py: installed the app --> put 'hats_rest.apps.HatsApiConfig' inside INSTALLED_APPS
 - .urls.py: put path('api/', include("hats_rest.urls")) and import include function

on hats.api.hats_rest:
 - .models.py: wrote LocationVO class and Hat class inside
 - .importing those two classes in models.py, and using three encoders(LocationVODetailEncoder, HatListEncoder, and HatDetailEncoder based on the encoders settled inside wardrobe.api.common.json.py), made api_show_hat and api_list_hats views