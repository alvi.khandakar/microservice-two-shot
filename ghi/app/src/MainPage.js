
import React from 'react';
import { Link } from 'react-router-dom';

// function MainPage() {
//   return (
//     <div className="px-4 py-5 my-5 text-center">
//       <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
//       <div className="col-lg-6 mx-auto">
//         <p className="lead mb-4">
//           Need to keep track of your shoes and hats? We have
//           the solution for you!
//         </p>
//       </div>
//     </div>
//   );
// }

function HatColumn(props) {
  return (
    <div className="col">
      {props.list.map(data => {
        const hat = data.hat;
        return (
          <div key={hat.id} className="card mb-3 shadow">
            <img src={hat.picture_url} className="card-img-top" alt=""/>
            <div className="card-body">
              <h5 className="card-title">{hat.style_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {hat.location.closetName}
              </h6>
              <p className="card-text">
                {hat.color}
              </p>
            </div>
          </div>
        );
      })}
    </div>
  );
}

function ShoeColumn(props) {
  return (
    <div className="col">
      {props.list.map(data => {
        const shoe = data.shoe;
        return (
          <div key={shoe.id} className="card mb-3 shadow">
            <img src={shoe.picture_url} className="card-img-top" alt=""/>
            <div className="card-body">
              <h5 className="card-title">{shoe.model_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {shoe.bin.closet_name}
              </h6>
              <p className="card-text">
                {shoe.color}
              </p>
            </div>
          </div>
        );
      })}
    </div>
  );
}

class MainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hatColumns: [[], [], []],
    };
  }

  async componentDidMount() {
    const url = 'http://localhost:8090/api/hats/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        // Get the list of hats
        const data = await response.json();

        // Create a list of for all the requests and
        // add all of the requests to it
        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090/api/hats/${hat.id}`;
          requests.push(fetch(detailUrl));
        }

        // Wait for all of the requests to finish
        // simultaneously
        const responses = await Promise.all(requests);

        // Set up the "columns" to put the hats
        // information into
        const hatColumns = [[], [], []];

        // Loop over the hat detail responses and add
        // each to to the proper "column" if the response is
        // ok
        let i = 0;
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json();
            hatColumns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatResponse);
          }
        }

        // Set the state to the new list of three lists of
        // hats
        this.setState({ hatColumns: hatColumns });
      }
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
          <h1 className="display-5 fw-bold">WARDROBIFY</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              The only resource you'll ever need to plan an run your hat/shoes for yourself.
            </p>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add Your Hat</Link>
              <Link to="/shoes/new" className="btn btn-outline-secondary btn-lg px-4 gap-3">Add Your Shoes</Link>
            </div>
          </div>
        </div>
        <div className="container">
          <h2>Your Closet of Hats</h2>
          <div className="row">
            {this.state.hatColumns.map((hatList, index) => {
              return (
                <HatColumn key={index} list={hatList} />
              );
            })}
          </div>
        </div>
      </>
    );
  }
}

export default MainPage;

