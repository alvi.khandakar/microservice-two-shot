import React from "react";

class LocationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            closetName: "",
            sectionNumber: "",
            shelfNumber: "",
        };
        this.handleClosetNameChange = this.handleClosetNameChange.bind(this);
        this.handleSectionNumberChange = this.handleSectionNumberChange.bind(this);
        this.handleShelfNumberChange = this.handleShelfNumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.closet_name = data.closetName;
        data.section_number = data.sectionNumber;
        data.shelf_number = data.shelfNumber;
        delete data.states;

        const locationUrl = "http://localhost:8100/api/locations/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();

            const cleared = {
                closetName: "",
                sectionNumber: "",
                shelfNumber: "",
            };
            this.setState(cleared);
        }
    }

    handleClosetNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    handleSectionNumberChange(event) {
        const value = event.target.value;
        this.setState({ roomCount: value });
    }

    handleShelfNumberChange(event) {
        const value = event.target.value;
        this.setState({ city: value });
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/states/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ states: data.states });
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new location</h1>
                        <form onSubmit={this.handleSubmit} id="create-location-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleClosetNameChange}
                                    value={this.state.closetName}
                                    placeholder="Closet Name"
                                    required
                                    type="text"
                                    name="closet_name"
                                    id="closet_name"
                                    className="form-control"
                                />
                                <label htmlFor="ClosetName">Closet Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleSectionNumberChange}
                                    value={this.state.sectionNumber}
                                    placeholder="Section number"
                                    required
                                    type="number"
                                    name="section_number"
                                    id="section_number"
                                    className="form-control"
                                />
                                <label htmlFor="section_number">Section Number</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleShelfNumberChange}
                                    value={this.state.shelfNumber}
                                    placeholder="Shelf number"
                                    required
                                    type="number"
                                    name="shelf_number"
                                    id="shelf_number"
                                    className="form-control"
                                />
                                <label htmlFor="shelf_number">Shelf Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default LocationForm;
