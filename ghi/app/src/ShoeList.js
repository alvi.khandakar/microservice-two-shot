import React from "react"


function ShoesList(props) {

  function refreshPage() {
    window.location.reload(false);
  }

  const deleteShoe = async (shoeId) => {
    fetch(`http://localhost:8080/api/shoes/${shoeId}/`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json"
      }
    })
    refreshPage()
  }



  if (props.shoes === undefined) {
    return null
  }
  return (
    <>
    <div><p></p></div>
    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
      <a type="button" className="btn btn-primary btn-lg px-4 gap-3" href="http://localhost:3000/shoes/new">Add Shoe</a>
    </div>
    <table className= "table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model</th>
          <th>Color</th>
          <th>Bin</th>
          <th>Image</th>
        </tr>
      </thead>
      <tbody>
        {props.shoes.map((shoe) => {
          return (
            <tr key={shoe.id}> 
              <td>{shoe.manufacturer}</td>
              <td>{shoe.model_name}</td>
              <td>{shoe.color}</td>
              <td>{shoe.bin}</td>
              <td><img src={shoe.picture_url} className="img-thumbnail" width="100px" height="100px" alt=""/></td>
              <td>
                <button type="button" className= "btn btn-danger" value= {shoe.id}
                onClick={() => deleteShoe(shoe.id)}>Delete</button>
              </td>

            </tr>
          )
        })}
      </tbody>
    </table>
    </>
  )
}

export default ShoesList