import React from "react";
import { Link } from 'react-router-dom';

// function HatsList(props) {
//     const [hats, setHats] = React.useState([]);
//     console.log("THIS IS PROPS.HATS LOL ====D", props.hats)
//     const deleteItem = (id) => async () => {
//         const url = `http://localhost:8090/api/hats/${id}`;
//         const fetchConfig = {
//             method: "delete"
//         }
//         const response = await fetch(url, fetchConfig)
//         if (response.ok) {
//             const deleted = await response.json();
//         }
//         setHats((hats) => hats.filter(item => {
//             return item.id !== id
//         }));
//     }
//     React.useEffect(() => { 

//         async function getHats() {
//             const url = `http://localhost:8090/api/hats/`;
//             const fetchConfig = {
//                 method: "GET",
//                 credentials: "include",
//             };
//             const response = await fetch(url, fetchConfig);
//             const JSONer = response.json()
//             console.log("....... JSONer ----- x", JSONer)
//             setHats(JSONer.hats);
//         }
//         getHats()
//     }, [hats]);
//     console.log("AY==========", hats)

function HatsList(props) {

    function refreshPage() {
        window.location.reload(false);
    }

    const deleteHat = async (hatId) => {
        fetch(`http://localhost:8090/api/hats/${hatId}/`, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json"
            }
        })
        refreshPage()
    }

    // console.log(hats)
    if (props.hats === undefined) {
        return null;
    }
    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Fabric</th>
                        <th>Style Name</th>
                        <th>Color</th>
                        <th>Location</th>
                        <th>Picture</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {props.hats.map((hat) => {
                        return (
                            <tr key={hat.id}>
                                <td>{hat.fabric}</td>
                                <td>{hat.style_name}</td>
                                <td>{hat.color}</td>
                                <td>{hat.location}</td>
                                <td><img src={hat.picture_url} className="img-thumbnail" width="100px" height="100px" alt={hat.picture_url} /></td>
                                <td><button type="button" className = "btn btn-danger" onClick={ () => deleteHat(hat.id)}>Delete</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Create a new hat</Link>
            </div>
        </>
    )
}




export default HatsList;