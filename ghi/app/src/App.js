import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import HatsList from './HatsList';
import ShoesList from './ShoeList';
import ShoeForm from './ShoeForm';
import HatForm from './HatForm';
import MainPage from './MainPage';


function App(props) {
  
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatsList hats={props.hats} />} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoesList shoes={props.shoes}/>} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
          <Route path="hats">
            <Route index element={<HatsList hats={props.hats}/>} />
            <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}




export default App;
