import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';


const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadWardrobe() {
  let hatsData, shoesData
  const hatsResponse =  await fetch('http://localhost:8090/api/hats/');
  const shoesResponse = await fetch("http://localhost:8080/api/shoes/");

  if (hatsResponse.ok) {
    hatsData = await hatsResponse.json();
  }
  if (shoesResponse.ok) {
    shoesData = await shoesResponse.json();
  }

  root.render(
    <React.StrictMode>
      <App
      hats={hatsData.hats}
      shoes={shoesData.shoes} 
      />
    </React.StrictMode>
  );
}
loadWardrobe();



//Need to work on a report vitals function